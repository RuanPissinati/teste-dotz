/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  TouchableOpacity, 
  Modal
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class Finish extends Component{
    render(){
        if(this.props.hasMoney){
            return ( 
            <View>
                <Text style={styles.modalTitle}>Compra realizada!</Text>
                <Text style={styles.modalPrice}>R$ 14,86</Text>
                <Text style={styles.modalLabel}>Item</Text>
                <Text style={styles.modalTextOne}>Ração Magnus Premium</Text>
                <Text style={styles.modalLabel}>Loja</Text>
                <Text style={styles.modalTextOne}>Prezunic Botafogo</Text>
                <Text style={styles.modalLabel}>02 de Abril às 19:30</Text>
                <Text style={styles.modalTextTwo}>Você receberá uma confirmação de troca em até 48h.</Text>
            </View>);
        }
        return(
            <View>
            <Text style={styles.modalSubTitulo}>Saldo insuficiente</Text>
            <Text style={styles.modalText}>
                Você precisa ter R$ 14,86 na carteira para realizar essa troca. 
                Mas não se preocupe, temos diversas formas para você 
                colocar dinheiro.
            </Text>
        </View>
       
        );
    }
}

export default class HelloWorldApp extends Component {

    state = {
        modalVisible: false,
        hasMoney: false
    };

    openModal() {
        this.setState({modalVisible:true});
        this.setState({hasMoney:!this.state.hasMoney});
    } 

    closeModal() {
        this.setState({modalVisible:false});
    }

    render() {
        return (
            <Fragment>
            <StatusBar backgroundColor="#004191" barStyle="light-content" />
           
            <SafeAreaView style={{flex: 1}}>
                <Modal
                    visible={this.state.modalVisible}
                    animationType={'slide'}
                    onRequestClose={() => this.closeModal()}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.innerContainer}>
                            <View style={styles.boxCloseModal}>
                                <TouchableOpacity style={styles.btnCloseModal} onPress={() => this.closeModal()} >
                                    <Image source={require('buyRacao/img/fechar.png')}/>
                                </TouchableOpacity>
                            </View>
                           <Finish hasMoney={this.state.hasMoney}></Finish>
                        </View>
                    </View>
                </Modal>
                <View style={styles.header} >
                    <Image style={styles.back} source={require('buyRacao/img/back.png')}/>
                    <Text style={styles.productName}>
                        Ração Magnus Premium
                    </Text>
                </View>
                <View style={{flex: 4, backgroundColor: 'white'}} >
                    <View style={styles.morePhotos}>
                        <Image style={styles.iconeMoreFotos} source={require('buyRacao/img/fotos.png')}/>
                        <Text style={styles.labelMoreFotos}>5</Text>
                    </View>
                    <View style={styles.product}>
                        <View style={styles.boxImg}>
                            <Image style={styles.productImage} source={require('buyRacao/img/magnus.jpg')}/>
                        </View>
                        <View style={styles.boxPrices}>
                            <View style={styles.boxPriceLine}>

                                <View>
                                    <Text style={styles.currentPrice}>R$ 14,86</Text>
                                    <Text style={styles.oldPrice}>R$ 18,90</Text>

                                </View>
                                <View style={styles.priceOff}>
                                    <Text style={styles.priceOffText}>12% OFF</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.moreInfos} >
                    <Text style={styles.txtDisponivel}>5 de 5 disponíveis</Text>
                    <Text style={styles.txtOferta}>Oferta expira ás 16:30 de 22/05</Text>
                </View>
                <View style={styles.footer} >
                    <TouchableOpacity style={styles.btnComprar} onPress={() => this.openModal()} >
                        <Text style={styles.btnComprarText}>Comprar</Text>
                    </TouchableOpacity>
                </View>
         
            </SafeAreaView>
            </Fragment>
            
        );
    }
}

const styles = StyleSheet.create({
    modalContainer:{
        paddingLeft: 35,
        paddingRight: 35,
    },
    modalLabel:{
        color: "#8b8b8b",
        fontSize: 14,
        marginTop: 40,
    },
    modalText:{
        color: "#8b8b8b",
        fontSize: 18,
    },
    modalTextOne:{
        color: "#2d2d2d",
        fontSize: 18,
    },
    modalTextTwo:{
        color: "#2d2d2d",
        fontSize: 14,
    },
    innerContainer:{
        display: "flex"
    },
    boxCloseModal:{
        justifyContent: "flex-end",
        flex: 1,
        flexDirection: "row",
        height: 40,
        marginTop: 20,
        marginRight: -10,
    },
    btnCloseModal:{
        height: 40,
        width: 40
    },
    modalTitle:{
        fontSize: 45,
        lineHeight: 45,
        color: "#004191",
        marginBottom: 40,
        marginTop: 80,
        fontWeight: "bold"
    },
    modalSubTitulo:{
        fontWeight: "bold",
        color: "#2d2d2d",
        fontSize: 18,
        marginTop: 80,
    },
    modalPrice:{
        fontSize: 18,
        color: "#2d2d2d",
        fontWeight: 'bold'
    },
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    boxPagamento:{
        flex: 1,
        position: "absolute", top: 0, bottom: 0, left: 0, right: 0
    },
    body: {
        display: "flex",
        flexDirection: "column",
        position: 'relative',
        backgroundColor: Colors.white,
    },
    productName: {
        fontSize: 18, 
        fontWeight: 'bold'
    },
    header: {
        flex: 1, 
        backgroundColor: 'white',
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 4,
        paddingRight: 4,
    },
    product: {
        display: "flex",
        flex: 1,
        // border
        // borderWidth: 1,
        // borderStyle: 'solid',
        // borderColor: 'red',
        flexDirection: "column",
    },
    infos:{
        // borderWidth: 1,
        // borderStyle: 'solid',
        // borderColor: 'green',
        flex: 1,
    },
    footer:{
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    btnComprar:{
        display: "flex",
        height: 64,
        backgroundColor: "#004191",
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    btnComprarText:{
        color: "#fff",
        fontSize: 18, 
    },
    morePhotos:{
        width: 56,
        height: 32,
        borderRadius: 60,
        position: 'absolute',
        top: 50,
        right: 32,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#192d2d2d',
        justifyContent: "space-around",
        zIndex: 9,
        backgroundColor: "#FFF",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 5,
        paddingRight: 5,
    },
    iconeMoreFotos:{
        height: 14, 
        width: 14, 
    },
    back:{
      width: 40,
    },
    boxImg:{
        display: "flex",
        flex: 3,
        justifyContent: "center",
        alignItems: "center",
        // border
        // borderWidth: 1,
        // borderStyle: 'solid',
        // borderColor: 'green',
    },
    boxPrices:{
        flex: 1,
        display: "flex",
        paddingLeft: 20,
        paddingRight: 20,
    },
    boxPriceLine: {
        // border
        flex: 1,
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        flexDirection:"row",
        borderBottomColor:"#192d2d2d",
        borderBottomWidth: 1,
    },
    productImage:{
        flex: 1,
        width: 250,
    },
    currentPrice:{
        fontSize: 18,
        color: "#2d2d2d",
        fontWeight: 'bold'
    },
    oldPrice:{
        fontSize: 14,
        color: "#b2b2b2",
        textDecorationLine: 'line-through', 
        textDecorationStyle: 'solid'
    },
    priceOff:{
        display: "flex",
        width: 91,
        height: 32,
        borderRadius: 32,
        backgroundColor: "#004191",
        justifyContent:"center",
        alignItems: "center",
    }, 
    priceOffText:{
        color: "#fff",
        fontSize: 14,
        fontWeight: "bold"
    },
    moreInfos:{
        flex: 1,
        justifyContent: "center",
        paddingLeft: 20,
        paddingRight: 20, 
        borderBottomColor:"#192d2d2d",
        borderBottomWidth: 1,
        marginBottom: 20,
    },
    txtDisponivel:{
        fontSize: 14,
        color: "#004191"
    },
    txtOferta:{
        fontSize: 14,
        color: "#2d2d2d"
    },
    

});

